#!/bin/sh
set -e
    wait-for-it MMS_DB:5432
    python manage.py collectstatic --settings=settings.prod --no-input
    python manage.py makemigrations measurement_server --settings=settings.prod
    python manage.py migrate --settings=settings.prod

exec "$@"
