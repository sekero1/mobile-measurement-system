# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

# setting up PI as access point (NAT)
sudo apt -y install dnsmasq hostapd

configFile=setup_files/nat_config
configPath=/etc/dhcpcd.conf

if grep -q wlan0 "$configPath"; then
	echo Delete wlan0 from $configPath and try again if config is incorrect.
else
	cat $configFile >> $configPath
fi

# restart dhcpcd deamon
sudo service dhcpcd restart

# DHCP
sudo cp setup_files/dnsmasq.conf /etc/dnsmasq.conf
sudo systemctl reload dnsmasq

# config host software (hostapd)
sudo cp setup_files/hostapd.conf /etc/hostapd/hostapd.conf

## register
hostapdFile=/etc/default/hostapd
if grep -q hostapd/hostapd.conf "$hostapdFile"; then
	echo Delete DAEMON_CONF from $hostapdFile and try again if config is incorrect.
else
	echo DAEMON_CONF=\"/etc/hostapd/hostapd.conf\" > $hostapdFile
fi

## start
sudo systemctl unmask hostapd
sudo systemctl enable hostapd
sudo systemctl start hostapd

### check
#sudo systemctl status hostapd
#sudo systemctl status dnsmasq

# Routing + Masquerade
settingsFile=/etc/sysctl.conf
settingsFileTMP=/etc/sysctl.conf.tmp
sudo cat $settingsFile | sed -e "s/#net.ipv4.ip_forward=1/net.ipv4.ip_forward=1/" > $settingsFileTMP
sudo mv $settingsFileTMP $settingsFile

# masquerade for outbound traffic on eth0
sudo iptables -t nat -A  POSTROUTING -o eth0 -j MASQUERADE

# save ip tables rule
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"

# register iptables on boot
if grep -q iptables-restore "/etc/rc.local"; then
	echo iptables already mentioned in rc.local
else
	sed -i '/exit 0/i \ iptables-restore < /etc/iptables.ipv4.nat' '/etc/rc.local'
fi
