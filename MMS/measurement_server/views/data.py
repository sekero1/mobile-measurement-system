import datetime

from django.http import JsonResponse

from ..models.measurement import Measurement
from ..models.measurement import MeasurementData

from core.modules.sensors import SupportedSensors
from core.modules.mqtt_client import MQTTClient


def data_plot(request, id: int):
    """
    There are multiple possible modes

    1. The measurement has just started. Data is only in Database.
       Data will be streamed directly from the database without averaging
    2. The measurement is running for a longer time. In this case averaging
       will be perfomred to reduce the amount of data that has to be
       displayed all new datapoints are in the plot without averaging
       (handled in js)
    3. The measurement has been stopped. In this case the hole data set
       is displayed using binned averaging

    System settings stored in most recent Measurement database entry
    control the average calculation.

        - avg_bucket_size - time width for wich a binned time average is
            calculated (timestamps indicate the beginning of the bucket)
        - plot_period - indicates the plot duration in minutes measured
            backwards from most recent measurement value (bucket average is applied first)

    If the cutoff defined via plot_period intersects a time bucket the first timestamp exceeds the cutoff to meet the start time of the bucket.

    Args:
        request (HttpRequest): Request from user interface
        id (int): Measurement object primary key

    Returns:
        JsonResponse: A JSON formatted string containing a dictionary of plotting values including sensor identifiers, measurement_values, associated_sensor colors etc.

    """

    mqtt_client = MQTTClient()

    if Measurement.objects.count() > 0:
        measurement = Measurement.objects.last()
        avg_bucket_size = measurement.avg_bucket_size  # s
        plot_period = measurement.plot_period  # min
    else:
        avg_bucket_size = 0.0  # s
        plot_period = 0  # min

    # Prepare the output dictionary
    out = {"start_measure": ""}
    # Add all sensor types to the output dict
    for device in SupportedSensors.keys():
        if "sht85" == device.lower():
            out["sht"] = []
        else:
            out[device.lower()] = []

    for device in SupportedSensors:
        # Check if sensor type is in data and fetch sensor ids
        try:
            # This is prone to SQL code injection attack.
            sensor_cls = SupportedSensors[device]["model"]["base"]
            sensor_serializer = SupportedSensors[device]["sensor_serializer"]
            sensor_res_cls = SupportedSensors[device]["model"]["res"]
            sensor_res_cls_name = device.lower()
            sensor_res_srlzr_cls_name = SupportedSensors[device]["model"][
                "res_serializer"
            ]
            sensor_ids_of_sensor_type = (
                f"SELECT id FROM measurement_server_{sensor_res_cls_name} "
            )
            sensor_ids_of_sensor_type += f"WHERE sensor_id IN (SELECT id \
                                           FROM measurement_server_measurementdata \
                                           WHERE id = {id})"
            entry = sensor_cls.objects.raw(sensor_ids_of_sensor_type)
        except AttributeError:
            entry = None

        if entry is None or len(entry) == 0:
            continue

        for i in range(len(entry)):
            # For all fetched sensors obtain measurement data
            sensor = entry[i]
            qtys = []
            for f in sensor_res_cls._meta.get_fields():
                field_name = str(f).split(".")[-1]
                if field_name not in ["time", "results"]:
                    qtys += [field_name]

            # Compile sql request

            # Check if bucket averaging is requested
            if avg_bucket_size == 0:
                sql_cmd = "SELECT time AS avg_time, COUNT(time) AS time"
            else:
                sql_cmd = f"SELECT time_bucket('{avg_bucket_size} seconds', time) \
                            AS avg_time,\
                            COUNT(time) AS time"

            for qty in qtys:
                sql_cmd += f', AVG("{qty}") AS "{qty}"'

            result_cls = SupportedSensors[device]["model"]["res"]
            sql_cmd += f" FROM measurement_server_{sensor_res_cls_name}results "
            sql_cmd += "WHERE "

            # Check if plotting period boundaries were requests.
            if plot_period > 0:
                last_time_stamp = result_cls.objects.raw(
                    f"SELECT COUNT(time) AS time, MAX(time) AS max_time FROM measurement_server_{sensor_res_cls_name}results\
                    WHERE results_id = {sensor.id} AND \
                    results_id IN (SELECT id FROM measurement_server_{sensor_res_cls_name} \
                        WHERE sensor_id IN \
                        (SELECT id \
                        FROM measurement_server_measurementdata \
                        WHERE id = {id}))"
                )
                last_time = last_time_stamp[0].max_time - datetime.timedelta(
                    minutes=plot_period
                )
                sql_cmd += f"time > '{last_time}' AND "

            sql_cmd += f"results_id = {sensor.id} AND results_id IN \
                         (SELECT id FROM measurement_server_{sensor_res_cls_name} "
            sql_cmd += f"WHERE sensor_id IN (SELECT id \
                         FROM measurement_server_measurementdata \
                         WHERE id = {id})) "
            sql_cmd += "GROUP BY avg_time ORDER BY avg_time"

            # Request data from database.
            results = result_cls.objects.raw(sql_cmd)

            if len(results) == 0:
                continue

            # Reduce the total number of values to the maximum.
            # Number of values defined in the system settings.
            sensor_header = sensor_serializer(sensor, no_res=True).data

            # Define a list of results.
            for r in results:
                r.time = r.avg_time
            Avg = sensor_res_srlzr_cls_name(data=results, many=True)
            Avg.is_valid()

            if "sht85" == sensor_res_cls_name:
                out["sht"].append(sensor_header)
                out["sht"][-1]["results"] = Avg.data
            else:
                out[sensor_res_cls_name].append(sensor_header)
                out[sensor_res_cls_name][-1]["results"] = Avg.data

            # Check if sensor type is in data.
            out["start_measure"] = MeasurementData.objects.get(id=id).start_measure

    return JsonResponse(out, safe=False)


def data_last_current(request, n: int):
    """
    Return last n values of the most recent measurement.
    For more detail see :func:`measurement_server.views.data.data_last`.

    Args:
        request (HttpRequest): Request from user interface
        id (int): Measurement object primary key
        n (int): Number of most recent measurement values

    Returns:
        JsonResponse: If no measurement is present, only an error entry is returned.
        Otherwise, measurement values together with sensor information are wrapped in JSON.
    """
    if MeasurementData.objects.all().count() == 0:
        return JsonResponse({"error": "No measurements available."})

    id = MeasurementData.objects.last().id
    return data_last(request, id, n=n)


def data_last(request, id: int, n: int = 10):
    """
    Return last n values of a measurement with a given id.
    This is the goto function for online data monitoring or retrieving controll values.

    Args:
        request (HttpRequest): Request from user interface
        id (int):
        n (int, optional): Number of most recent measurement values.

    Returns:
        JsonResponse: If no measurement is present, only an error entry is returned.
        Otherwise, measurement values together with sensor information are wrapped in JSON.

    """
    if type(n) is not int:
        try:
            n = int(n)
        except ValueError:
            return JsonResponse({"error": "Measurement id unknown."})

    if type(id) is not int:
        try:
            id = int(id)
        except ValueError:
            return JsonResponse({"error": "Measurement id unknown."})

    if not MeasurementData.objects.filter(id=id).exists():
        return JsonResponse({"error": "Measurement id unknown."})

    data = MeasurementData.objects.get(id=id)
    out = {
        "name": data.name,
        "created": data.created,
        "start_measure": data.start_measure,
        "stop_measure": data.stop_measure,
        "stopped": data.stopped,
        "systime": datetime.datetime.now(),
    }

    for device in SupportedSensors:
        # check if sensor type is in data
        # Query to get all data
        # Query to get last n values
        if "sht85" == device.lower():
            out["sht"] = []
        else:
            out[device.lower()] = []

        try:
            # This is prone to SQL code injection attack
            sensor_cls = SupportedSensors[device]["model"]["base"]
            s = device.lower()

            sensor_ids_of_sensor_type = f"SELECT id FROM measurement_server_{s} \
                                          WHERE sensor_id IN \
                                          (SELECT id \
                                           FROM measurement_server_measurementdata \
                                           WHERE id = {id})"
            entry = sensor_cls.objects.raw(sensor_ids_of_sensor_type)
        except AttributeError:
            entry = None

        if entry is not None and len(entry) > 0:
            for i in range(len(entry)):
                sensor = entry[i]

                sql_cmd = f"SELECT * from measurement_server_{s}results \
                            WHERE results_id = {sensor.id} AND results_id IN \
                             (select id from measurement_server_{s} \
                              WHERE sensor_id = {id}) \
                            ORDER BY time DESC LIMIT {n}"

                result_cls = SupportedSensors[device]["model"]["res"]
                values = result_cls.objects.raw(sql_cmd)

                if len(values) == 0:
                    continue

                # Calc frequency
                t0 = values[0].time
                tn = values[len(values) - 1].time
                if (t0 - tn).total_seconds() != 0:
                    freq = (len(values) - 1.0) / (t0 - tn).total_seconds()
                else:
                    freq = 0.0

                # Write JSON output for serialized View.
                out_s = {
                    "Color": sensor.Color,
                    "COMport": sensor.COMport,
                    "FTDIserial": sensor.FTDIserial,
                    "MUXaddress": sensor.MUXaddress,
                    "MUXport": sensor.MUXport,
                    "SENSORidentifier": sensor.SENSORidentifier,
                    "SENSORserial": sensor.SENSORserial,
                    "Frequency": f"{freq:0.2f}",
                }

                for qty in SupportedSensors[device]["sensor"]._units:
                    out_s[qty] = []

                out_s["time"] = []

                for val in values:
                    out_s["time"].insert(0, getattr(val, "time"))
                    for qty in SupportedSensors[device]["sensor"]._units:
                        out_s[qty].insert(0, getattr(val, qty))

                if "sht85" == device.lower():
                    out["sht"].append(out_s)
                else:
                    out[device.lower()].append(out_s)

    return JsonResponse(out)
