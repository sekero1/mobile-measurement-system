import json
from rest_framework.decorators import api_view
from rest_framework.response import Response

from ..models.measurement import KnownSensors
from ..models.measurement import Calibration
from ..models.measurement import Units


@api_view(["GET", "POST"])
def calibrationView(request, serial_number=None):
    """
    Args:
        request (HttpRequest): Request from user interface
        serial_number(str, optional): Sensor serial number
    Returns:
        rest_framework.response.Response: On POST request a JSON dictionary with an ERROR or a SUCCESS key is returned.
        On GET sensor calibration values are returned as a string defining the calibration function.
        An empty list is returned if no calibrations are registered for that sensor.
        If the serial number is unknown an empty dictionary is returned.
        If no serial number is parsed a list of all available serial numbers is returned.
    """

    if request.method == "GET":
        if serial_number is None:
            serials = KnownSensors.objects.values_list("SENSORserial", flat=True)
            return Response(list(serials))

        sensor = KnownSensors.objects.filter(SENSORserial=serial_number)
        if sensor.exists():
            sensor = KnownSensors.objects.get(SENSORserial=serial_number)
            calibrations = sensor.calibration.all()
            calibration_str = [str(c) for c in calibrations]
            return Response({serial_number: calibration_str})

        return Response({})
    elif request.method == "POST":
        if not request.body:
            return Response({"ERROR": "Empty request"})

        try:
            data = json.loads(request.body)
        except json.JSONDecodeError:
            return Response({"ERROR": f"Invalid json string."})

        # Verify serial number information
        serial_number = data.get("SENSORserial", None)
        if serial_number is None:
            return Response({"ERROR": "Missing serial number."})

        sensor = KnownSensors.objects.filter(SENSORserial=serial_number)
        if not sensor.exists():
            return Response({"ERROR": "Unknown serial number."})
        sensor = sensor.first()

        # Verify unit information
        qty = data.get("qty", None)
        if qty is None:
            return Response({"ERROR": "Missing quantity."})
        unit = Units.objects.filter(qty=qty)
        if not unit.exists():
            return Response(
                {"ERROR": "No unit instance matching quantity information."}
            )
        unit = unit.first()

        # Set values x0 to x5 and check if any parameter is not None
        cal_kwargs = {}
        valid_paramter = False
        for i in range(6):
            cal_kwargs[f"x{i}"] = data.get(f"x{i}", None)
            if cal_kwargs[f"x{i}"] is not None:
                valid_paramter = True

        cal_kwargs["qty"] = unit
        sensor.add_calibration(**cal_kwargs)

        if not valid_paramter:
            return Response({"SUCCESS": "Unregistered calibraiton function."})

        return Response({"SUCCESS": "Registered new calibration function."})
