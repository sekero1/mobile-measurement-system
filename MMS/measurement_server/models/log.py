# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
Holds all models used to generate the API
and to communicate between Worker and GUI
"""

import json
from django.db import models
from django.utils.timezone import now


class Log(models.Model):
    """
    Event log which gives live information about the device state.
    """

    time = models.DateTimeField(default=now)
    message = models.CharField(max_length=220, default="")
    typ = models.CharField(max_length=20, default="")

    def save(self, *args, mqtt=None, **kwargs):
        super().save(*args, **kwargs)
        if mqtt is not None:
            topic = "log/" + self.typ
            msq = {"time": str(self.time), "message": self.message}
            mqtt.publish(topic, json.dumps(msq))
