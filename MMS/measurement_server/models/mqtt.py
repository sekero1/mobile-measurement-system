# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT


from django.db import models


class MQTT(models.Model):
    """
    MQTT Brker connection information
    """

    # Device topic
    topic = models.CharField(max_length=220, default="mms")
    # Server
    host = models.CharField(max_length=220)
    port = models.IntegerField(default=1883)
    # Auth
    user = models.CharField(max_length=220, blank=True)
    password = models.CharField(max_length=220, blank=True)
    # Status
    connected = models.BooleanField(default=False)
