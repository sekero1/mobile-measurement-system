from django.db import models


class Units(models.Model):
    qty = models.CharField(max_length=25, default="")
    sign = models.CharField(max_length=25, default="")
    name = models.CharField(max_length=40, default="")

    def __str__(self):
        return str(self.qty) + " [" + str(self.sign) + "]"
