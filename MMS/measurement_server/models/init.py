# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
Holds all models used to generate the API
and to communicate between Worker and GUI
"""

from django.db import models
from .measurement import MQTTModel


class Init(MQTTModel):
    """
    Holds informaiton about the initialization of sensors and ftdi devices.
    """

    maintopic = "init"
    STATE_CHOICES = [
        ("idle", "IDLE"),
        ("ok", "OK"),
        ("fail", "Failed"),
        ("run", "Running"),
    ]

    # Indicate if a initialization is requested
    init = models.BooleanField(default=False)
    # Indicates if ftdi initialization was successfull.
    init_ftdi = models.BooleanField(default=False)
    # Indicates if com initialization was successfull.
    init_com = models.BooleanField(default=False)
    # Indicates if comdevices initialization was successfull.
    init_com_devices = models.BooleanField(default=False)
    # Indicates if sensor initialization was successfull.
    init_sensors = models.BooleanField(default=False)
    # Returns information about the overall
    # state/result of the initialization process.
    state = models.CharField(max_length=4, choices=STATE_CHOICES, default="idle")
    error = models.CharField(max_length=220, default="")
    # Holds information about the ftdi initialization process
    ftdi_state = models.CharField(max_length=4, choices=STATE_CHOICES, default="idle")
    ftdi_error = models.CharField(max_length=220, default="")
    # Holds information about the comport initialization process
    com_state = models.CharField(max_length=4, choices=STATE_CHOICES, default="idle")
    com_error = models.CharField(max_length=220, default="")
    # Total number of accessible i2c/spi ports
    total_ports = models.IntegerField(default=0)
    # Number of scanned ports
    scanned_ports = models.IntegerField(default=0)
    # Number of detected sensors (updated during scan)
    detected_sensors = models.IntegerField(default=0)
    # Number of sensors that have been initialized
    # successfully (sensor.exists executed without error notice)
    sensors_initialized = models.IntegerField(default=0)
