import decimal

from django.db import models
from django.db.backends import utils
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from .units import Units


class Calibration(models.Model):
    time = models.DateTimeField(auto_now_add=True)
    x5 = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=2)
    x4 = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=2)
    x3 = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=2)
    x2 = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=2)
    x1 = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=2)
    x0 = models.DecimalField(null=True, default=None, max_digits=5, decimal_places=2)
    qty = models.ForeignKey(Units, on_delete=models.PROTECT)
    readonly = models.BooleanField(default=False)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    sensor = GenericForeignKey("content_type", "object_id")

    def __str__(self):
        str_polynomial = ""
        coeffs = [self.x5, self.x4, self.x3, self.x2, self.x1, self.x0]
        for i, coeff in enumerate(coeffs):
            if coeffs[i] is None:
                continue

            if coeff is not None and coeff != 0:
                if coeff > 0 and str_polynomial != "":
                    str_polynomial += "+"
                str_polynomial += str(coeff)
                power = len(coeffs) - 1 - i
                if power == 0:
                    pass
                elif power == 1:
                    str_polynomial += "x"
                else:
                    str_polynomial += f"x**{power}"
        if str_polynomial == "":
            return "No valid calibration coefficients."
        return f"{self.qty}: cal(x) = {str_polynomial}"

    def __bool__(self):
        coeffs = [self.x5, self.x4, self.x3, self.x2, self.x1, self.x0]
        for coeff in coeffs:
            if coeff is not None and coeff != 0:
                return True
        return False

    def eval(self, value, max_digits=None, decimal_places=None):
        """evaluate calibration equation"""
        if not bool(self):
            return value

        corrected_value = 0
        coeffs = [self.x5, self.x4, self.x3, self.x2, self.x1, self.x0]
        for i, coeff in enumerate(coeffs):
            if coeff is not None and value is not None:
                power = len(coeffs) - i - 1
                corrected_value += float(coeff) * float(value) ** power

        corrected_value = decimal.Decimal(corrected_value)
        if max_digits is not None or decimal_places is not None:
            try:
                utils.format_number(corrected_value, max_digits, decimal_places)
            except (decimal.InvalidOperation, TypeError, ValueError):
                return

        return corrected_value
