# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.urls import resolve
from django.test import TestCase
from django.http import HttpRequest

import datetime
import json

from .test_helpers import genMeasurement
from .test_helpers import get_time_string
from measurement_server.views.data import data_plot

from measurement_server.models.measurement import Measurement


class DataViewsTest(TestCase):
    def setUp(self):
        # This range is chosen to be compatible
        # with all sensor measurement value database fields
        values = range(0, 200, 10)

        t0 = datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc)
        dt = datetime.timedelta(minutes=1)
        timestamps = [t0]

        for i in range(len(values) - 1):
            timestamps.append(timestamps[-1] + dt)

        # Store values
        data, sensor_models, result_models = genMeasurement(
            n_sensors=1, timestamps=timestamps, values=values
        )

        self.data = data
        self.sensor_models = sensor_models
        self.result_models = result_models
        self.values = values
        self.timestamps = timestamps
        self.dt = dt

    def test_no_data_available(self):
        # Test no result case
        # Get plot data
        response = self.client.get("/api/last/123/")
        data = response.json()
        self.assertTrue("error" in data.keys())
        self.assertTrue("id unknown" in data["error"])

        response = self.client.get(f"/api/last/123/10/")
        data = response.json()
        self.assertTrue("error" in data.keys())
        self.assertTrue("id unknown" in data["error"])

    def test_bad_requests(self):
        response = self.client.get("/api/last/abc/1/")
        data = response.json()
        self.assertTrue("error" in data.keys())
        self.assertTrue("id unknown" in data["error"])

        response = self.client.get(f"/api/last/{self.data.id}/abc/")
        data = response.json()
        self.assertTrue("error" in data.keys())
        self.assertTrue("id unknown" in data["error"])

    def test_data_response(self):
        n = 0
        response = self.client.get(f"/api/last/{self.data.id}/{n}/")
        data = response.json()
        for sensor_model in self.sensor_models:
            sensor_type = sensor_model.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"

            self.assertIn(sensor_type, data)
            self.assertEqual(0, len(data[sensor_type]))

        n = 1
        response = self.client.get(f"/api/last/{self.data.id}/{n}/")
        data = response.json()
        for sensor_model in self.sensor_models:
            sensor_type = sensor_model.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"
            self.assertIn(sensor_type, data)
            for unit in sensor_model.objects.first().units.all():
                self.assertEqual(len(data[sensor_type][0][unit.qty]), n)
                self.assertEqual(float(data[sensor_type][0][unit.qty][0]), 190)
                self.assertEqual(float(data[sensor_type][0][unit.qty][-1]), 190)

        n = 10
        response = self.client.get(f"/api/last/{self.data.id}/{n}/")
        data = response.json()
        for sensor_model in self.sensor_models:
            sensor_type = sensor_model.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"
            self.assertIn(sensor_type, data)
            for unit in sensor_model.objects.first().units.all():
                self.assertEqual(len(data[sensor_type][0][unit.qty]), n)
                self.assertEqual(float(data[sensor_type][0][unit.qty][0]), 100)
                self.assertEqual(float(data[sensor_type][0][unit.qty][-1]), 190)

        # n > number of values in database
        n = 200
        response = self.client.get(f"/api/last/{self.data.id}/{n}/")
        data = response.json()
        for sensor_model in self.sensor_models:
            sensor_type = sensor_model.__name__.lower()
            if "sht85" == sensor_type:
                sensor_type = "sht"
            self.assertIn(sensor_type, data)
            for unit in sensor_model.objects.first().units.all():
                self.assertEqual(len(data[sensor_type][0][unit.qty]), len(self.values))
                self.assertEqual(float(data[sensor_type][0][unit.qty][0]), 0)
                self.assertEqual(float(data[sensor_type][0][unit.qty][-1]), 190)
