import random
from pytz import timezone
from measurement_server.models.measurement import KnownSensors
from measurement_server.models.measurement import MeasurementData
from measurement_server.models.measurement import Units
from measurement_server.models.measurement import PlotSettings
from core.modules.sensors import SupportedSensors
from datetime import timedelta
from datetime import datetime
from unittest.mock import mock_open
from unittest import mock


def get_time_string(t):
    tz = timezone("Europe/Berlin")
    return t.astimezone(tz).strftime("%Y-%m-%d %H:%M:%S.%f%z")


def mapped_mock_exists(file_list):
    """Create a mock "open" that will mock open multiple files based on name
    Args:
        file_contents_dict: A dict of 'fname': 'content'
    Returns:
        A Mock opener that will return the supplied content if name is in
        file_contents_dict, otherwise the builtin open
    """

    def new_path_exists(fname, *args, **kwargs):
        if fname in file_list:
            return True
        else:
            return False

    mock_exists = mock.Mock()
    mock_exists.side_effect = new_path_exists
    return mock_exists


builtin_open = open


def mapped_mock_open(file_contents_dict):
    """Create a mock "open" that will mock open multiple files based on name
    Args:
        file_contents_dict: A dict of 'fname': 'content'
    Returns:
        A Mock opener that will return the supplied content if name is in
        file_contents_dict, otherwise the builtin open
    """
    mock_files = {}
    for fname, alt_fname in file_contents_dict.items():
        mock_files[fname] = builtin_open(alt_fname)

    def my_open(fname, *args, **kwargs):
        if fname in mock_files:
            return mock_files[fname]
        else:
            return builtin_open(fname, *args, **kwargs)

    mock_opener = mock.Mock()
    mock_opener.side_effect = my_open
    return mock_opener


def getRandomIdentifier(n):
    random_string = ""
    for _ in range(10):
        random_integer = random.randint(65, 90)
        # Keep appending random characters using chr(x)
        random_string += chr(random_integer)
    return random_string


def genMeasurement(n_sensors=2, n_results=2, timestamps=None, values=None):
    # Generate measurement object and fill database
    sensorCounter = 0

    data = MeasurementData.objects.create(
        name="unit_test_export_" + str(int(random.random() * 100))
    )
    data.save()

    sensor_models = []
    result_models = []
    for sensor in SupportedSensors:
        sensor_models.append(SupportedSensors[sensor]["model"]["base"])
        result_models.append(SupportedSensors[sensor]["model"]["res"])

    signs = []
    qties = []
    for sensor in SupportedSensors:
        signs += SupportedSensors[sensor]["sensor"]._units.values()
        qties += SupportedSensors[sensor]["sensor"]._units.keys()

    # Deduplication
    qties = list(dict.fromkeys(qties))

    for qty, sign in zip(qties, signs):
        if not Units.objects.filter(name=qty):
            Units.objects.create(name=qty, qty=qty, sign=sign)

    for sensor_model, result_model in zip(sensor_models, result_models):
        for i in range(n_sensors):
            sensor_serial = getRandomIdentifier(i * 2)
            sensor_identifier = getRandomIdentifier(i * 2)
            sen = sensor_model.objects.create(
                SENSORserial=sensor_serial,
                SENSORidentifier=sensor_identifier,
                sensor=data,
                SENSORtype=sensor_model.__name__,
            )
            ks = KnownSensors.objects.create(
                SENSORserial=sensor_serial,
                SENSORidentifier=sensor_identifier,
                SENSORtype=sensor_model.__name__,
                connected=True,
            )
            sensorCounter += 1
            for qty in qties:
                if hasattr(result_model, qty):
                    sen.units.add(Units.objects.get(qty=qty))
                    ks.units.add(Units.objects.get(qty=qty))
                    plot = PlotSettings(qty=qty, show=True)
                    plot.save()
                    ks.plots.add(plot)
            ks.save()
            sen.save()

    dt = 0  # microseconds
    for sensor_model, result_model in zip(sensor_models, result_models):
        for sensor in sensor_model.objects.all():
            if timestamps is not None:
                for i in range(len(timestamps)):
                    if values is not None:
                        assert len(timestamps) == len(values)
                        v = values[i]
                    else:
                        v = random.random() * 100

                    res = result_model.objects.create(
                        results=sensor, time=timestamps[i] + timedelta(microseconds=dt)
                    )
                    for qty in qties:
                        if hasattr(result_model, qty):
                            setattr(res, qty, v)
                    res.save()
                dt += 1

            else:
                for j in range(n_results):
                    res = result_model.objects.create(results=sensor)
                    for qty in qties:
                        if hasattr(result_model, qty):
                            setattr(res, qty, random.random() * 100)
                    res.save()

    return data, sensor_models, result_models
