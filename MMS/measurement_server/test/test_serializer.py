# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

# Standard packages
from django.utils.timezone import localtime
import datetime
from django.test import TestCase

# MMS specifics
## DB models
from measurement_server.models.measurement import SHT85results
from measurement_server.models.measurement import SCD30results

## Result model serializer
from measurement_server.serializers import SHT85resultsSerializer
from measurement_server.serializers import SCD30resultsSerializer


class ResultSerializerTestCase(TestCase):
    """
    Check if Serializers work correctly
    """

    help = "Control LEDs and Buttons"

    def test_btn(self):
        """Check if datetime does not strip ms information if ms=0"""
        # timestamp with all elements populated
        t = datetime.datetime(
            year=2020, month=1, day=24, hour=12, minute=1, second=2, microsecond=2
        )
        res_sht_all = SHT85results(temperature=10, humidity=10, time=t)
        res_scd30_all = SCD30results(temperature=10, humidity=10, co2=20, time=t)

        # timestamp without ms
        t = datetime.datetime(year=2020, month=1, day=24, hour=12, minute=1, second=2)
        res_sht_noms = SHT85results(temperature=10, humidity=10, time=t)
        res_scd30_noms = SCD30results(temperature=10, humidity=10, co2=20, time=t)

        # timestamp without s but ms
        t = datetime.datetime(
            year=2020, month=1, day=24, hour=12, minute=1, microsecond=2
        )
        res_sht_nos = SHT85results(temperature=10, humidity=10, time=t)
        res_scd30_nos = SCD30results(temperature=10, humidity=10, co2=20, time=t)

        # timestamp without s and ms
        t = datetime.datetime(year=2020, month=1, day=24, hour=12, minute=1)
        res_sht_nosms = SHT85results(temperature=10, humidity=10, time=t)
        res_scd30_nosms = SCD30results(temperature=10, humidity=10, co2=20, time=t)

        scd30_stamps = [res_scd30_all, res_scd30_noms, res_scd30_nos, res_scd30_nosms]
        sht_stamps = [res_sht_all, res_sht_noms, res_sht_nos, res_sht_nosms]

        string_lengths = []
        for scd, sht in zip(scd30_stamps, sht_stamps):
            string_lengths.append(len(SCD30resultsSerializer(scd).data["time"]))
            string_lengths.append(len(SHT85resultsSerializer(sht).data["time"]))

        # Check if all strings have the same length
        self.assertEqual(string_lengths[:-1], string_lengths[1:])
