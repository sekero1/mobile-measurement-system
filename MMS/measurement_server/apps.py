# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

from django.apps import AppConfig


class MeasurementServerConfig(AppConfig):
    name = "measurement_server"
