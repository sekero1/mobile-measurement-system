// SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
// SPDX-License-Identifier: MIT

export function sortTable(table, n) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  switching = true;
  // Set the sorting direction to ascending:
  dir = "asc";
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table.rows;
    /* Loop through all table rows (except the
    first, which contains table headers): */
    for (i = 1; i < (rows.length - 1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].getElementsByTagName("TD")[n];
      if (x.childElementCount == 0) {
          x = x.innerHTML.toLowerCase() 
      } else {
          x = x.children[0]
          if (x.tagName == "INPUT") {
              x = x.value
          } else if(x.tagName == "SPAN") {
              x = x.innerHTML
          } else {
              console.log("Unsupported sorting tag.")
          } 
      }

      y = rows[i + 1].getElementsByTagName("TD")[n];
      if (y.childElementCount == 0) {
          y = y.innerHTML.toLowerCase() 
      } else {
          y = y.children[0]
          if (y.tagName == "INPUT") {
              y = y.value
          } else if(y.tagName == "SPAN") {
              y = y.innerHTML
          } else {
              console.log("Unsupported sorting tag.")
          }
      }

      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x > y) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x< y) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } else {
      /* If no switching has been done AND the direction is "asc",
      set the direction to "desc" and run the while loop again. */
      if (switchcount == 0 && dir == "asc") {
        dir = "desc";
        switching = true;
      }
    }
  }
}

export function sortMeasurementTable(el, toggle) {
  var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
  table = $("#"+el.getAttribute("quantity")+"Table");
  switching = true;
  var n = el.getAttribute("idx")
                                                                           
  // Hide all other arrows (indicates deactivated sort)
  table.find("thead th svg").attr("hidden", true)
  table.find("thead th").attr("active", false)
  el.setAttribute("active", true)
                                                                           
                                                                           
  var arrow = $('#' + el.id+ ' svg')[0];
  arrow.removeAttribute("hidden")
  // Get sorting direction
  dir = el.getAttribute("direction");
  if (dir == "none" && toggle) {
    dir = "desc"
    arrow.classList.replace("fa-sort", "fa-sort-up")
  } else if (toggle && dir == "asc") {
    dir = "desc"
    arrow.classList.replace("fa-sort-down", "fa-sort-up")
  } else if (toggle && dir == "desc") {
    dir = "asc"
    arrow.classList.replace("fa-sort-up", "fa-sort-down")
  }
  el.setAttribute("direction", dir)
  /* Make a loop that will continue until
  no switching has been done: */
  while (switching) {
    // Start by saying: no switching is done:
    switching = false;
    rows = table[0].rows;
    /* Loop through all table rows (except the
    first, which contains table headers and footers): */
    for (i = 1; i < (rows.length-1); i++) {
      // Start by saying there should be no switching:
      shouldSwitch = false;
      /* Get the two elements you want to compare,
      one from current row and one from the next: */
      x = rows[i].cells[n]
      if (x.id =="sensor-default") {
          continue
      }
      y = rows[i + 1].cells[n]
      if (y.id =="sensor-default") {
          y = rows[i + 2].cells[n]
      }

      x = x.getElementsByClassName("sensor-slug")[0];
      y = y.getElementsByClassName("sensor-slug")[0];
      if (typeof x == 'undefined') {
        x = rows[i].cells[n];
        y = rows[i + 1].cells[n];
      }
      /* Check if the two rows should switch place,
      based on the direction, asc or desc: */
      if (dir == "asc") {
        if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      } else if (dir == "desc") {
        if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
          // If so, mark as a switch and break the loop:
          shouldSwitch = true;
          break;
        }
      }
    }
    if (shouldSwitch) {
      /* If a switch has been marked, make the switch
      and mark that a switch has been done: */
      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
      switching = true;
      // Each time a switch is done, increase this count by 1:
      switchcount ++;
    } 
  }
}
