# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

import re
from django import forms
from django.core.exceptions import ValidationError


class AlphaNumericField(forms.CharField):
    """
    A field to assure that measurement names do not
    contain unexpected characters
    """

    def clean(self, value):
        value = super(AlphaNumericField, self).clean(value)
        if not re.match(r"[_A-z0-9]+", value):
            raise ValidationError("AlphaNumeric characters  and _ only.")
        return value
