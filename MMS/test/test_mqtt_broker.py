# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

import paho.mqtt.client as mqtt


class Messages:
    @staticmethod
    def connect():
        print("MMS connected to Broker.")

    @staticmethod
    def publish():
        print("Test data")


if __name__ == "__main__":
    client = mqtt.Client(client_id="MMS01")
    client.on_connect = Messages.connect()
    client.on_publish = Messages.publish()
    client.connect("192.168.0.153", 1883, 60)
    client.publish("mms/01/", 27.03)
    client.loop()
