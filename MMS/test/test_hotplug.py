# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

import pyftdi
import pyftdi.usbtools as usbtools
from usb.core import USBError
import time
import usb

while True:
    try:
        number_of_connected_ftdi = len(
            usbtools.UsbTools.find_all([(0x0403, 0x6014)], nocache=True)
        )
    except USBError:
        usbtools.UsbTools.flush_cache()
        time.sleep(0.05)
        number_of_connected_ftdi = 0
    except ValueError:
        usbtools.UsbTools.flush_cache()
        time.sleep(0.05)
        number_of_connected_ftdi = 0

    print(number_of_connected_ftdi)
    time.sleep(0.05)
