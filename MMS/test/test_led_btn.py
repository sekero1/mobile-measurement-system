# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

"""
This is a interface to the FTDI and sensor hardware. All state changes
are initiated by a change of a data model instance. All data model
instances are shared with the measurement_server which provides a GUI.
"""

# Python standards
import time
from django.utils.timezone import localtime
import os
import datetime
import shutil
from json import JSONDecodeError

# check if on Raspberry pi
if os.uname()[4][:3] == "arm":
    from gpiozero import Button
    from gpiozero import LED
else:
    raise AttributeError("Case operation not possible for non Pi systems")


# Button configuration.
PIN_LED_INIT = 7
PIN_BUTTON_INIT = 5
PIN_LED_MEASUREMENT = 27
PIN_BUTTON_MEASUREMENT = 10


class test_led_btn(object):
    """
    Main commands for service worker
    """

    help = "Control LEDs and Buttons"

    def setup_pins(self):
        """
        Configure LEDs and Buttons.
        """
        self.led_init = LED(PIN_LED_INIT)
        self.btn_init = Button(PIN_BUTTON_INIT)
        self.led_measurement = LED(PIN_LED_MEASUREMENT)
        self.btn_measurement = Button(PIN_BUTTON_MEASUREMENT)

    def test_led(self):
        """
        check if all leds are connected correctly
        """
        self.led_measurement.on()
        time.sleep(1)
        self.led_init.on()
        time.sleep(1)
        self.led_measurement.off()
        time.sleep(1)
        self.led_init.off()

    def test_btn(self):
        """
        check if all buttons are connected correctly
        """
        self.led_measurement.on()
        self.btn_measurement.wait_for_press()
        print("measurement pressed")
        self.led_measurement.off()
        self.led_init.on()
        self.btn_init.wait_for_press()
        print("init pressed")
        self.led_init.off()


case = test_led_btn()
case.setup_pins()
case.test_led()
print("led tes done")
case.test_btn()
