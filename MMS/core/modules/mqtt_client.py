# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

# MQTT Client
import paho.mqtt.client as mqtt
from measurement_server.models.mqtt import MQTT


class MQTTClient:
    """
    Main communication module to interface with a MQTT Broker
    """

    topic = ""

    def on_publish(self, client, userdata, result):
        pass

    def on_connect():
        pass

    def publish(self, topic: str, data: dict):
        """
        Send data to broker.

        Args:
            topic (str): UTF-8 string used by brokers to filter messages.
            data (dict): Payload send to broker.

        Returns:
            None

        """
        topic = self.topic + topic
        if self.client is not None:
            self.client.publish(topic, data)

    def connect(self):
        """
        Connect to the MQTT broker using the database entry
        with the highest primary key. If no setup is available
        None is returned else a mqtt.Client instance is returned.
        In case of failure during the initialization of the connection
        the database entry `connected` is set to ``False`` which is ``True``
        in case of a success.
        """
        if MQTT.objects.count() == 0:
            return None

        mqtt_setup = MQTT.objects.last()

        if mqtt_setup is None:
            return None

        client = mqtt.Client(client_id=mqtt_setup.topic)

        try:
            client.connect(mqtt_setup.host, mqtt_setup.port, 60)
            client = client
            client.on_publish = self.on_publish
            client.on_connect = self.on_connect
            if not mqtt_setup.connected:
                mqtt_setup.connected = True
                mqtt_setup.save()
        except (ConnectionRefusedError, OSError):
            client = None
            if mqtt_setup.connected:
                mqtt_setup.connected = False
                mqtt_setup.save()

        self.topic = mqtt_setup.topic
        if self.topic[-1] != "/":
            self.topic += "/"

        return client

    def __init__(self):
        self.client = self.connect()
