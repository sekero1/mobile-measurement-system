## Welcome to the mobile measurement system.
This software has been developed to serve as a free and open-source plattform to realize measurement set-ups quickly and reliably.
While a web interface is provided for quick measurements, a RESTful API and a MQTT client is included to integrate the system into a larger setting.
An extensive documentation is presented in the [documentation](https://mobile-measurement-system.readthedocs.io/en/latest/)

In the following a brief overview of the installation steps is given.

### Installation

Prepare your system
```
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install docker
sudo apt-get install docker-compose -qq -y
sudo addgroup docker
sudo usermod -aG docker $USER
sudo apt-get install git -y -qq
sudo service docker start
sudo systemctl enable docker
```

On some systems docker might not be available. Check the internet for guidance!

Copy the code base 
```
git clone https://gitlab.dlr.de/as-boa/mobile-measurement-system.git
```

Register FTDI rules
```
cd mobile-measurement-system/setup
bash ./install_usb.sh
```

Download and start the container stack:
```
cd ..
sudo docker-compose up -d
```

If you want to use external start and init buttons:
```
sudo docker-compose -f docker-compose.yml -f docker-compose-pi.yml up -d
```

Make sure 4 containers with an identivier MMS_* are up and running (no restarting)
```
sudo docker ps
```

The MMS is installed and can be accessed on the operating machine via the browser using http://localhost or via other devices on the network via http://<host-ip>/
Sensor datasheets can be found in the documentation section of the web interface. 
For further details checkout the [documentation](https://mobile-measurement-system.readthedocs.io/en/dev/) pages of this repository.

### Update (online system)
Updating a mobile measurement system with access to the internet to a new release is done via:
```
# pull newest image
docker pull niehaus/mms:<version>
cd <directory of docker-compose.yml>
docker-compose up -d 
```
For the newest version replace <version> with latest. For specific release tags checkout [docker hub](https://hub.docker.com/r/niehaus/mms/tags?page=1&ordering=last_updated).
When using shared IP adresses you might be prompted that your quota is exceeded.
Workaround: Signup at docker hub and after confirming your account enter your credentials after executing ``` docker login ``` in your terminal. 

### Update (offline system):
You need a system with internet access in order to download the docker image. Note that beforehand docker has to be installed on the system. For downloading the image use:
``` 
docker pull niehaus/mms:<version> 
```
Consequently you need to save the image to file
``` 
docker save -o mmsOfflineImage.img niehaus/mms:<version> 
```
Now transfer the file to your offline system and load the image 
``` 
docker load -i mmsOfflineImage.img 
```
Update the version of your docker-compose on your offline system and run the update
``` 
docker-compose up -d 
```


### Install dev environment:
Clone the project and cd into the project directory
```
git clone https://gitlab.dlr.de/as-boa/mobile-measurement-system.git
cd mobile-measurement-system
```
You need to fullfill the prerequisites detailed in [Installation](README.md#Installation)
Additionally install Python >3.8, Postgres dev (libraries + headers) and docker like so:
```
sudo apt-get install python3-dev build-essential docker python3-pip libpq-dev # Debian / Ubuntu
sudo yum install python3-devel python3-pip docker libpq-devel # Red Hat / CentOS
```
Make sure to setup a virtual environment and then run
```
pip install -r MMS/requirements.txt
```
Fire up the database
```
./postgres.sh
```
Start the development web server
```
./dev.sh start
```

### Running tests
Make sure your development environment is running smoothly.
Tests are executed via
``` 
cd MMS
./test.sh dev
```

### Raspberry Pi push buttons
For quick starting measurements, hardware buttons can be added to the GPIOs of a Rapsberry PI header. 
The corresponding container can be installed via
``` docker-compose -f docker-compose.yml -f docker-compose-pi.yml ```

The pins are identical throughout all Pi versions.
If the use of buttons is activated, buttons have to be connected.
* The initialization button has to be connected to GPIO 7 and GND.
* The measurement button has to be connected to GPIO 27 and GND.

Led indicators are all optional. 
* The init indicator has to be wired to GPIO 7 and GND.
* The measurement indicator has to be connected to GPIO 10 and GND.

To start the initialization process, push the initialization button.
The initialization indicator will start to blink (1/2Hz).
When the initialization process has finished successfully, the blinking stops and the initialization led is continuously active. 
To start a measurement, the initialization process has to be finished.
If a measurement is ready to be started, the measurement led is active. 
After pushing the measurement button the measurement starts and the measurement indicator will blink (1/2Hz).
To stop a measurement, push the button again. 
The blinking will stop and the measurement ends.
During measurements the init button is deactivated.
Vice versa the measurement button is deactivated when the system performs an initialization.
The web interface reflects all state changes evoked by the push buttons.
Measurements are labeled according to the system time and can be found in the 'Data' tab of the 'Settings' menu.

When using a RPi 4 make sure to add proper pull up resistors to both buttons. 


### Wifi Client
A wifi access point for Raspberry Pis can be installed on the system level using [the installation script in the setup directory.](setup/install_pi_ap.sh). 
Default parameters:
- Gateway/Device IP: 192.168.4.1
- SSID: YOUR_SSID
- Password: YOUR_SECURE_PASSWORD
- max. 20 dhcp clients

These settings can be changed by modifying [hostapd](setup/setup_files/hostapd.conf), [dnsmasq](setup/setup_files/dnsmasq.conf) and [nat_config](setup/setup_files/nat_config.conf).
Note that you have to change the SSID and the password before installing the access point.

### SAMBA share 
The fragment result files can be shared via samba. This has been tested on Debian based systems (Raspbian Buster, Ubuntu 20,  Linux Mint 20)
Build using
```
docker-compose -f docker-compose.smb.yml up -d
```
Make sure that the file inside the compose-File is matching your result directory.
Measurement values are stroed in *mobile-measurement-system/MMS/measurement_server/media/results*
Also make sure that no other service sues the SMB-Port.
You can check this via:
```
systemctl status smbd.service
```
You can disable the servce via:
```
systemctl stop smbd.service
systemctl disable smbd.service
```

### Resetting the entire system

Stop all container and remove
```
docker-compose rm -vfs
```
Remove volumes
```
docker volume prune
```
Build new docker stack
```
docker-compose up -d -f <only if a non defaul .yml is needed>
```


### Also usefull:
After cloning images shrink using:
```
https://github.com/Drewsif/PiShrink
```

For sensor debugging check
```
docker ps
docker logs MMS_SENSOR
docker run -it --net=mms2_default --privileged mms2_mms_sensor bash
```
