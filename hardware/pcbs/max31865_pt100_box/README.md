USB to 16 x MAX31865 4-wirte PT100 Measurement Box
==================================================


![](Hardware/max31865_16_pt100_front.png)
![](Hardware/max31865_16_pt100_back.png)

16 channel PT100 measurement transducer

Repository Contents
-------------------

* **/Hardware** - Files to be sent to pcb manufacturer
* **/Production** - Project data files

Documentation
--------------
With an assembled pcb breadboard you need to connect a PT100 4-wire RTD to the 4 pin sockets. 
Connect pin 1 and 3 to  one side of the RTD while 2 and 4 are wired to the opposite site.
At 0°C the resistance between pairs 1-2 and 3-4 should read 100Ohm while 1-3 and 2-4 shoule be below 10Ohm.
Please have in mind that a 430Ohm reference resistor is used and the resistance you want to measure should be lower than a quarter of the reference resistance. 
For other applications or different resistance based sensors feel free to swap the reference resistor.
For further details on the RTD-SPI interface refer to the [MAX31865 Datashet](https://datasheets.maximintegrated.com/en/ds/MAX31865.pdf).

PCB manufactureres we worked with (and have stencils ready)

    - www.richter-pforzheim.de
