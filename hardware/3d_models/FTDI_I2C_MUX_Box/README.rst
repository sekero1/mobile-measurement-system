For a FTDI MUX Box you need an FT232H by Adafruit (Product id 2264) together with a TCA9548A Qwiic Mux Breakout.
Since 5V I2C communication is used you cannot use the Qwiic connector on the FTDI.
Take a short Qwiic wire, cut it in half and connect 


======= === ===== ============
Qwiic       FTDI  Function
======= === ===== ============
Yellow  <-> D0    CLK
Yellow  <-> D7    CLK Stretch
Blue    <-> D1    SDA
Blue    <-> D2    SDA
Red     <-> 5V    Vin
Black   <-> GND 
======= === ===== ============

To connect to the host device you additionally need a USB type C connector.

