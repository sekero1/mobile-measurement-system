MQTT
====================

.. automodule:: core.modules.mqtt_client
   :members:

.. autoclass:: measurement_server.models.measurement.MQTTModel
   :noindex:

.. automethod:: measurement_server.forms.mqttSettingsForm
