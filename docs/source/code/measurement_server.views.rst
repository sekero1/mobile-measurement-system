System Interfaces
=================================

API 
---

.. automodule:: measurement_server.views.calibration
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: measurement_server.views.data
   :members:
   :undoc-members:
   :show-inheritance:

GUI
---

.. automodule:: measurement_server.views.gui
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: measurement_server.views.legacy_data_live
   :members:
   :undoc-members:
   :show-inheritance:

.. automodule:: measurement_server.views.settings
   :members:
   :undoc-members:
   :show-inheritance:
