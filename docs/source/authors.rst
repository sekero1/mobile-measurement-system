Authors
-------

Main developer
~~~~~~~~~~~~~~~

 * Konstantin Niehaus

Contributors
~~~~~~~~~~~~~~~~
*alphabetical order*

 * Brinkema, Robert
 * Hermann, Alexander 
 * Lange, Pascal 
 * Werner, Felix
 * Westhoff, Andreas

