.. _references:

References
==========

A list of research projects or publications which employed the mobile measurement system.



Publications
------------

**English**

* Quantitative microbial risk assessment for aerosol transmission of SARS-CoV-2 in aircraft cabins based on measurement and simulations `Link <https://reports.nlr.nl/bitstream/handle/10921/1568/NLR-CR-2021-232-RevEd-1.pdf?sequence=5&isAllowed=y>`__
* Generation, Detection and Analysis of Aerosol Spreading Using low-cost Sensors `Link <https://elib.dlr.de/141666/>`__
* Analysis of aerosol dispersion in the Do728 passenger compartment `Link <https://elib.dlr.de/141645/>`__
* Characterization of a Mixed Convection Cell Designed for Phase Transition Studies in Moist Air `Link <https://doi.org/10.1007/978-3-030-79561-0_46>`__.
* Experimental Simulation of the Human Respiration `Link <https://elib.dlr.de/136336/>`__
* On the impact of a low-momentum ventilation system with an air purifier unit on indoor aerosol-dynamics  `Link <https://elib.dlr.de/142910/>`__
* DLR investigations on aerosol dispersion in aircraft and trains `Link <https://elib.dlr.de/143010/>`__

**German**

* Aerosolausbreitung in der DO-728-Passagierkabine `Link <https://elib.dlr.de/140963/>`__
* Aerosolausbreitung in Flugzeugkabinen und Personenzügen `Link <https://elib.dlr.de/142910/>`__
* Demonstrator für Innovationen im Reisendenkomfort und Klimatisierung - DIRK `Link <https://elib.dlr.de/140607/>`__

Projects and Experimental facilities
------------------------------------

* Next Generation Class Room `Link <https://www.dlr.de/content/en/articles/news/2020/04/20201103_dlr-tests-filter-system-to-reduce-virus-load-in-enclosed-spaces.html>`__
* Next Generation Car `Link <https://www.dlr.de/content/de/grossforschungsanlagen/generisches-zuglabor-goettingen.html?nn=39e3faf0-d8fd-4818-9632-39f1b8fd3e8d>`__
* ADVENT - Lower energy consumption through innovative ventilation systems in long-range aircraft `Link <https://www.dlr.de/content/en/articles/news/2020/02/20200527_dlr-studies-the-spread-of-viruses-in-aircraft-and-trains.html>`__ , `Link <https://www.dlr.de/as/en/desktopdefault.aspx/tabid-17350/27473_read-69992/>`__
* Modular Cabin Mockup (MKG) `Link <https://www.dlr.de/content/de/grossforschungsanlagen/modulares-kabinen-mockup-goettingen-mkg.html?nn=39e3faf0-d8fd-4818-9632-39f1b8fd3e8d>`__
* Gener­ic Train Lab­o­ra­to­ry Göt­tin­gen (GZG) `Link <https://www.dlr.de/content/en/research-facilities/generic-train-laboratory-goettingen_gzg.html>`__





