Developers
----------

Setup development environment
``````````````````````````````

Clone the project and navigate into the project directory

.. code-block:: shell

    git clone https://gitlab.dlr.de/as-boa/mobile-measurement-system.git
    cd mobile-measurement-system

You need to fullfill the prerequisites detailed in :ref:`software setup <softwaresetup>`.
Additionally, install Python >3.8, Postgres dev (libraries + headers) and docker like so:

.. code-block:: shell

    sudo apt-get install python3-dev build-essential docker python3-pip libpq-dev # Debian / Ubuntu
    sudo yum install python3-devel python3-pip docker libpq-devel # Red Hat / CentOS

Make sure to setup a virtual environment and then run

.. code-block:: shell

    pip install -r MMS/requirements.txt

You can use

.. code-block:: shell

    ./dev.sh start

to start, restart or stop a fresh development environment with a clean database running in a container called timescaledb. 
The timescaledb database is started in the `` ./postgres.sh `` script.


Tests
```````

Switch to the virtual environment of your development setup and start the local postgres database 
via ``./postgres.sh``.
Tests are executed by

.. code-block:: shell

    cd MMS
    ./test.sh dev

If you want to test a function under development you can find snippets 
for isolated testing in the comments of `test.sh`.


