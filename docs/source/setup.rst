Setup
=====

To get the Mobile Measurement System up and running you need to
install its software and to connect the measurement equipment.
First you need to get the software up an running. 

.. _softwaresetup:
.. include:: setup/software.inc

.. _hardwaresetup:
.. include:: setup/hardware.inc
