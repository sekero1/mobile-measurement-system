Software update
```````````````

To specify the new software version change the `MMS_VERSION` variable in your 
`.env` file to equal your desired version. Use the `latest` tag to refer to the most recent stable release
associated to the `main`-Branch. 
Use the `dev` tag to get the current devolopment version.
An overview of all release tags can be found on `Docker Hub Registry <https://hub.docker.com/r/niehaus/mms/tags?page=1&ordering=last_updated>`_.
If you want to ensure consistency or to avoid incompatiblity issues due to i.e. api-deprecations
make sure not to use `latest` and pin the version instead.

Online systems
..............

Updating a mobile measurement system with access to the internet to a new release via:

.. code-block:: shell

    cd <directory of docker-compose.yml>
    docker-compose pull
    docker-compose up -d 

.. note::
    When using shared IP adresses you might be prompted that your docker quota is exceeded.
    **Workaround**: Signup at `Docker Hub <https://hub.docker.com/>`_ and after confirming your account run ``docker login`` in your terminal and enter your credentials.  
    You now have a personal quota.

Offline systems
...............

Switch to an online system download the desired docker image. 
Note that you need docker on that online system to be installed, too.
For downloading the image use 

.. code-block:: shell

    docker pull niehaus/mms:<version> 

where ``<version>`` shall be replaced with your intended version tag.

Consequently, you need to save the image to file

.. code-block:: shell

    docker save -o mmsOfflineImage.img niehaus/mms:<version> 

Now transfer the file to your offline system and load the image 

.. code-block:: shell

    docker load -i mmsOfflineImage.img 

Update `MMS_VERSION` in the `.env` file on your offline system and run the update

.. code-block:: shell

    docker-compose up -d 
    docker-compose restart


