Uninstall
``````````

Stop all container and remove

.. code-block:: shell

    docker-compose rm -vfs

Remove unused volumes

.. code-block:: shell

    docker volume prune

Delete /var/lib/mms via

.. code-block:: shell

    sudo rm -rf /var/lib/mms


