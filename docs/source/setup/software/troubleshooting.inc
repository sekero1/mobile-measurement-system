Troubleshooting
```````````````

Use ``docker ps`` to check if all containers are running. 
If a container keeps restarting check out the 

.. code-block:: shell

    docker logs <CONTAINER NAME>

For the container responsible for reading data from measurement devices use

.. code-block:: shell

    docker logs MMS_SENSOR

To navigate inside a running container and to execute commands from within use

.. code-block:: shell

    docker run -it --net=mms2_default --privileged <CONTAINER NAME> bash

