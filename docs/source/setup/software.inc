Software installation
-----------------------

.. include:: /setup/software/architectures.inc
.. include:: /setup/software/install.inc
.. include:: /setup/software/update.inc
.. include:: /setup/software/uninstall.inc
.. include:: /setup/software/troubleshooting.inc

.. _raspberry:

.. include:: /setup/software/raspberry.inc
