Cables
`````````
In general it is adviced to keep cable connections as short as
possible to reduce the risk of electromagnetic interferences (EMI).
Further it is adviced to favor longer USB cables over long I2C cables.
Cables that were used successfully:

.. list-table::
   :header-rows: 1

   * - Part
     - Supplier
     - ID
   * - Qwiic - Qwiic 500mm
     - Digi key
     - 1568-1713-ND
   * - Qwiic - Qwiic 100mm
     - Digi key
     - 1528-4210-ND
   * - SHT85 - Qwiic
     - ESTO
     - custom made
   * - SFM/SDP - Qwiic
     - ESTO
     - custom made


