
Periphery
`````````

To connect I2C to the Host devices USB port a FTDI232 is used in
combination with an I2C multiplexer to allow for identical sensor types on 
the same I2C bus. Bill of materials (1 mux box):

.. list-table::
   :header-rows: 1

   * - Part
     - Supplier
     - ID
     - Note
   * - FT232H
     - Digi Key
     - 1528-1449-ND
     - Any breakout board will work
   * - BOB-16784
     - Digi Key
     - 1568-BOB-16784-ND
     - All TCA9548A boards will work
   * - Qwiic Cable to Wire
     - Digi Key
     - 1568-PRT-17260-ND 
     - Cut in half and solder to FTDI232 Breakout board
.. Note:: In the future a PCB with FTDI and TCA9548A would make manual soldering obsolete and would reduce its dimensions.


