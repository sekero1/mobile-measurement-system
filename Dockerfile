# SPDX-FileCopyrightText: 2022 German Aerospace Center (DLR)
#
# SPDX-License-Identifier: MIT

FROM python:3.9-slim-buster
LABEL maintainer="Konstantin Niehaus konstantin.niehaus@dlr.de"

ENV APP_ROOT /src
ENV CONFIG_ROOT /config
ENV DATABASE_URL db

# Install install packages for UI and Sensor Reader
RUN apt-get -qq -y update --fix-missing
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get -qq -y upgrade
RUN apt-get install -y -qq \
    gcc \
    git \
    gunicorn \
    libpq-dev \
    python-dev \
    usbutils \
    wait-for-it \
    libhdf5-serial-dev \
    netcdf-bin \
    libnetcdf-dev

# Setup main directory
RUN mkdir ${APP_ROOT}
WORKDIR ${APP_ROOT}

# Install python dependencies
COPY ./MMS/requirements.txt ${CONFIG_ROOT}/requirements.txt

RUN pip install -r ${CONFIG_ROOT}/requirements.txt

# copy/add mms
COPY ./MMS/ ${APP_ROOT}
COPY ./setup/setup_files/entrypoint_mms_server.sh ${APP_ROOT}/entrypoint_mms_server.sh
COPY ./setup/setup_files/entrypoint_mms_test_server.sh ${APP_ROOT}/entrypoint_mms_test_server.sh

# Register git commit
COPY ./.git/ /tmp/.git
RUN echo "$(cd /tmp/.git && git log -1 --decorate)" > ${APP_ROOT}/mms_version
RUN rm -r /tmp/.git
